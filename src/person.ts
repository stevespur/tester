export class Person {
    private readonly age: number;

    public constructor(age: number) {
        this.age = age;
        this.age = age;
    }

    public isEighteenOrOver(): boolean {
        return this.age >= 18;
    }

    public isEighteen(): boolean {
        return this.age == 18;
    }

    public isMinor(): boolean {
        return this.age < 18;
    }

    public isOverTwentyOne(): boolean {
        return this.age >= 21;
    }

    public canFoo(): boolean {
        if(this.age >= 18 && this.age <= 65) {
            return true;
        }

        return false;
    }

    public canBar(): boolean {
        if(this.age < 18 || this.age > 65) {
            return true;
        }

        return false;
    }
}
