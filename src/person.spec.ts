import {Person} from './person';

describe('Person', () => {
    describe('isEighteenOrOver', () => {
        test("should return true when 18", () => {
            const person = new Person(18);
            expect(person.isEighteenOrOver()).toBe(true);
        });
    });

    describe('isEighteen', () => {
        test('should return true when 18', () => {
            const person = new Person(18);
            expect(person.isEighteen()).toBe(true);
        });
    });

    describe('isMinor', () => {
        test('should return false when 18', () => {
            const person = new Person(18);
            expect(person.isMinor()).toBe(false);
        });
    });

    describe('isOverTwentyOne', () => {
        test('should return false when 18', () => {
            const person = new Person(18);
            expect(person.isOverTwentyOne()).toBe(false);
        });
    });

    describe('canFoo', () => {
        test('should return true when 18', () => {
            const person = new Person(18);
            expect(person.canFoo()).toBe(true);
        });
    });

    describe('canBar', () => {
        test('should return false when 18', () => {
            const person = new Person(18);
            expect(person.canBar()).toBe(false);
        });
    });
});
