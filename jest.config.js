module.exports = {
    roots: ['<rootDir>/src'],
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    // testResultsProcessor: "<rootDir>/node_modules/ts-jest/coverageprocessor.js",
    collectCoverage: true,
    collectCoverageFrom: [
        "src/**/*.{ts,tsx}"
    ],
    coverageReporters: [
        "lcov"
    ]
};
